
compile: 
	gcc main.c -o out
	gcc pthreads.c -lpthread -o out2
	chmod +x grapths.py

pthread: 
	./out2 -p 150 -i 50 -c 75 -d 20 -s 100
	./grapths.py 
seq:
	./out -p 150 -i 50 -c 75 -d 20 -s 100
	./grapths.py
output:
	./out2 -p 300 -i 25 -c 99 -d 60 -s 40
	./grapths.py
