                                       RESULTADOS SIMULACION                                           
WEEKS   PERCENT IMMUNE   PERCENT INFECTED   TOTAL PEOPLE   HEALTHY PEOPLE   SICK PEOPLE   IMMUNE PEOPLE       
0       0.00             0.00               300            300              0             0                   
1       0.00             3.33               300            290              10            0                   
2       0.00             3.33               300            290              10            0                   
3       0.00             3.33               300            290              10            0                   
4       0.00             3.67               300            289              11            0                   
5       0.00             3.67               300            289              11            0                   
6       0.00             3.67               300            289              11            0                   
7       0.00             4.00               300            288              12            0                   
8       0.00             4.00               300            288              12            0                   
9       0.00             4.67               300            286              14            0                   
10      0.00             4.67               300            286              14            0                   
11      0.00             5.33               300            284              16            0                   
12      0.00             5.67               300            283              17            0                   
13      0.00             5.67               300            283              17            0                   
14      0.00             5.67               300            283              17            0                   
15      0.00             6.00               300            282              18            0                   
16      0.00             6.00               300            282              18            0                   
17      0.00             6.33               300            281              19            0                   
18      0.00             6.33               300            281              19            0                   
19      0.00             7.33               300            278              22            0                   
20      0.00             8.00               300            276              24            0                   
21      0.00             8.33               300            275              25            0                   
22      0.00             9.00               300            273              27            0                   
23      0.00             9.33               300            272              28            0                   
24      0.00             9.67               300            271              29            0                   
25      0.00             10.00              300            270              30            0                   
26      0.00             10.67              300            268              32            0                   
27      0.00             11.33              300            266              34            0                   
28      0.00             12.67              300            262              38            0                   
29      0.00             12.67              300            262              38            0                   
30      0.00             14.00              300            258              42            0                   
31      0.00             15.67              300            253              47            0                   
32      0.00             17.33              300            248              52            0                   
33      0.00             18.33              300            245              55            0                   
34      0.00             20.33              300            239              61            0                   
35      0.00             21.67              300            235              65            0                   
36      0.00             23.00              300            231              69            0                   
37      0.00             25.00              300            225              75            0                   
38      0.00             26.00              300            222              78            0                   
39      0.00             27.00              300            219              81            0                   
40      0.00             28.67              300            214              86            0                   
