# Tarea-Sincronizacion

Introduccion:

El programa se encarga de simular el escenario de virus de netlogo. Para 
comparar ambas simulaciones se utiliza una interfaz grafica generada
apartir del archivo cvs_out el cual es leido por un programa en python el cual
transforma el txt en la grafica que indica las personas o 'turtles' infectados,
las inmunes, las sanas y los turtles totales. 
Por otro lado se tiene 2 versiones del programa, una version serial y una 
paralelizada. La version serial esta codificada en main.c y la paralelizada en
pthreads.c 


Prerequisitos:

Se deben descargar todos los archivos incluidos en este git, entre ellos:
- main.c
- grapths.py
- pthreads.c
- Makefile
- cvs_out.txt
- sim_out.txt

Para correr el programa:

1. Se debe compilar usando el comando 'make compile'
2. Si se quiere ver la grafica se corre 'make pthread' o 'make seq'

*NOTA: seq corre la grafica serial y pthread corre la grafica paralelizada

Para cambiar los parametros del programa dentro del make:

- -p: cantidad de personas
- -i: porcentaje de infecciosidad
- -c: porcentaje de posibilidad de recuperacion
- -d: duracion de la enfermedad en semanas
- -s: duracion de la simulacion en semanas

