#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <stdbool.h>
#include <time.h>
#include <pthread.h>
#include <assert.h>

#define MAX_POP 300
#define LIFESPAN 50*52
#define chance_reproduce 10
#define IMMUNITY_DURATION 52
#define CARRYING_CAPACITY 300
#define DEAD 1
#define IMMUNE 0
#define NOT_IMMUNE 2
#define NUMBER_THREADS 8

/* Global Variables */
double porcent_infected;
double porcent_immune;
int number_people;
int chance_recover;
int duration; 
int infectiousness; 
int immune_counter;
int infected_counter;
int alive_turtles;
int simulation_duration;
pthread_mutex_t lock;
/*Struct for each turtle and their information*/
struct turtle {
    bool sick; //turtle is sick
    int remaining_immunity; //how long is turtle immune
    int sick_time; //Says how long turtle has been sick
    int age; 
    //Extra
    int x_loc;
    int y_loc;
    bool alive; //Says if turtle is alive 
}; 

struct turtle turtles[MAX_POP];
//Struct contains ranges for the threads to manage in parallel
struct thread_data {
    int low_range;
    int high_range; 
};
//Function makes a healthy turtle sick
//Input [in]: turtle to get sick
void get_sick (struct turtle * turtles) {
    turtles->sick = true; 
    turtles->remaining_immunity = 0;
} 
/* Fuction used to setup a number of turles at the start of the program
    input [in] turtles is the pointer to the whole array of turtles to set up*/
void setup_turles (struct turtle * turtles) {
    for(int i = 0; i < number_people; i++) {
        turtles[i].x_loc = rand()%100;
        turtles[i].y_loc = rand()%100;
        turtles[i].age = rand()%LIFESPAN;
        turtles[i].sick_time = 0;
        turtles[i].remaining_immunity = 0;
        turtles[i].alive = true; 
        turtles[i].sick = false; 
    }
    for(int i = number_people; i< MAX_POP; i++) {
        turtles[i].alive = false;
        turtles[i].sick = false;
    }
    //Randomly get 10 turtles sick
    for(int i = 0; i < 10; i++) {
        get_sick(&turtles[rand()%number_people]);
        
    }
}
//Turtles get healthy 
void get_healthy (struct turtle * turtles) {
    turtles->sick = false; 
    turtles->remaining_immunity = 0;
    turtles->sick_time = 0;
}

//turtles can become immune for a set period of time if they survive disease
void become_immune (struct turtle * turtles) {
    turtles->sick = false; 
    turtles->sick_time = 0;
    turtles->remaining_immunity = IMMUNITY_DURATION;
}
/* Turtles get older every week. If they are old enough they die
    else their parameters are updated. */
void get_older (struct turtle * turtles) {
    turtles->age = turtles->age + 1;
    if(turtles->age > LIFESPAN) {
        turtles->alive = false; 
    } else {
        if(turtles->remaining_immunity > 0) {
            turtles->remaining_immunity--;  
        } 
        if(turtles->sick) {
            turtles->sick_time++;
        }
    }
    
}
//Fuction moves a turtle randomly on the grid 35x35
void move_turtle(struct turtle * turtles) {
    turtles->x_loc = rand()%35;
    turtles->y_loc = rand()%35;
}
//fuction returns if a turtle is immune or not to disease
int check_immunity(struct turtle turtles) {
    if(turtles.remaining_immunity > 0) {
        return IMMUNE;
    } else return NOT_IMMUNE;
}
//function takes in a sick turtle and infects nearby turtles on the same coordinates by chance
void infect(struct turtle * turtles, struct turtle * sick_turtle) {
    for (int i = 0 ; i < MAX_POP; i++) {
        if (!turtles[i].sick) {   
            if(sick_turtle->x_loc == turtles[i].x_loc && sick_turtle->y_loc == turtles[i].y_loc && check_immunity(turtles[i]) == NOT_IMMUNE) {
                if(rand()%100 < infectiousness) {
                    get_sick(&turtles[i]);
                }
            }
        }
    }
}

//Fuction initializes a new turtle with new parameters
void hatch(struct turtle * turtles) {
    for (int i = 0; i < MAX_POP; i++) {
        if(!turtles[i].alive) {
            //Set up turtle as a new turtle with age 1
            turtles[i].alive = true;
            turtles[i].age = 1;
            //Make new turtle healthy
            get_healthy(&turtles[i]);
            turtles[i].x_loc = rand()%35;
            turtles[i].y_loc = rand()%35;
            //Increment total of alive turtles
            break;
        }
    }
}

//turtles reproduce randomly by chance 
void reproduce(struct turtle * turtles) {
    if (alive_turtles < MAX_POP && ((float)rand()/RAND_MAX)*100 < 1) {
        for (int i = 0; i < MAX_POP; i++) {
        if(!turtles[i].alive) {
            //Set up turtle as a new turtle with age 1
            turtles[i].alive = true;
            turtles[i].age = 1;
            //Make new turtle healthy
            get_healthy(&turtles[i]);
            turtles[i].x_loc = rand()%35;
            turtles[i].y_loc = rand()%35;
            break;
        }
    }
    }
}
/* Fucntion randomly decides if a turtle will recover from a disease 
    or die if its been sick long enough
    input: [in] turtles contains the turtle that is sick. 
*/
int recover_or_die(struct turtle* turtles) {
    if(turtles->sick_time > duration) {
        if(((float)rand()/RAND_MAX)*100 < chance_recover) {
            //if turtle survives virus and recoves he becomes immune
            become_immune(turtles);
            return IMMUNE;
        } else {
            //turtles dies if it doeesnt surive
            turtles->alive = false; 
            return DEAD;
        }

    }
    return 0;
}
/* Fuction simulation is used by threads to execute the simualation in parallel;
    depending on the number of threads each thread would manage a different amount
    of turtles. Some sections of code are blocked since they use global variables. 

    Input: [in] thread_args structue contains threads information to manage
*/
void *simulation(void * thread_args) {
    struct thread_data *data; 
    data = (struct thread_data *) thread_args; 
    int low_range = data->low_range;
    int high_range = data->high_range;
    for(int i = low_range; i < high_range ; i++) {
        if(turtles[i].alive){
                get_older(&turtles[i]);
                move_turtle(&turtles[i]);
                if(turtles[i].sick) {
                    recover_or_die(&turtles[i]);
                    pthread_mutex_lock(&lock);
                    infect(turtles, &turtles[i]);
                    pthread_mutex_unlock(&lock);
                }
                else {
                    pthread_mutex_lock(&lock);
                    reproduce(turtles);
                    pthread_mutex_unlock(&lock);
                }
            }
    }
    pthread_exit(NULL);
}
//Function counts sick turtles, turtle must be alive to be sick, updates infected counter
void * count_sick() {
    for (int i = 0; i < MAX_POP; i++)
    {
        if(turtles[i].alive && turtles[i].sick) {
            infected_counter++;
        }
    }
    pthread_exit(NULL);
}
//Counts turtles alive
void * count_alive() {
    for (int i = 0; i < MAX_POP; i++)
    {
        if(turtles[i].alive) alive_turtles++;
    }
    pthread_exit(NULL);
}
//Counts turtles alive and immune, update immune counter
void * count_immune() {
    for (int i = 0; i < MAX_POP; i++)
    {
        if(turtles[i].remaining_immunity > 0 && turtles[i].alive) {
            immune_counter++;
        }
    }
    pthread_exit(NULL);
}


int main(int argc, char * argv[]) {
    struct thread_data thread_data_array[NUMBER_THREADS];
    pthread_t thread[NUMBER_THREADS];
    int current_weeks = 0;
    immune_counter = 0;
    infected_counter = 0;
    //Results to validate correct creation and join of threads
    int join_result;
    int create_result;
    FILE *csv_out;
    FILE *f_out;
    //Archivos de salida
    csv_out = fopen("cvs_out.txt", "w");
    f_out = fopen("sim_out.txt", "w");
    //Random seed for simulation
    srand(time(NULL));
    //Set up constants
    if (pthread_mutex_init(&lock, NULL) != 0) { //Set up mutex lock
        printf("\n mutex init has failed\n"); 
        return 1; 
    }
    //Input parameters
    if (argc > 1) {
        number_people = atoi(argv[2]);
        infectiousness = atoi(argv[4]);
        chance_recover = atoi(argv[6]);
        duration = atoi(argv[8]);
        //Duration of simulation es in weeks
        simulation_duration = atoi(argv[10]); 
    }
    //Set up each thread boundaries
    for(int i = 0; i < NUMBER_THREADS; i++) {
        thread_data_array[i].low_range = i*75;
        thread_data_array[i].high_range =i*75+75;
    } 
    //Write out titles for output files
    fprintf(csv_out, "WEEKS,PORCENT IMMUNE,PORCENT INFECTED,TOTAL PEOPLE,HEALTHY PEOPLE,SICK PEOPLE,IMMUNE PEOPLE\n");
    fprintf(f_out, "                                       RESULTADOS SIMULACION                                           \n");
    fprintf(f_out, "%-7s %-16s %-18s %-14s %-16s %-13s %-20s\n","WEEKS","PERCENT IMMUNE","PERCENT INFECTED","TOTAL PEOPLE","HEALTHY PEOPLE","SICK PEOPLE","IMMUNE PEOPLE");
    
    alive_turtles = number_people; 
    porcent_immune = (float)immune_counter/(float)alive_turtles;
    porcent_infected = (float)infected_counter/(float)alive_turtles;
    //Write out week 0 of output file 
    fprintf(csv_out, "%d,%f,%f,", current_weeks, porcent_immune,porcent_infected );
    fprintf(csv_out, "%d,%d,%d,%d\n", alive_turtles, alive_turtles-infected_counter-immune_counter,infected_counter,immune_counter);
    fprintf(f_out, "%-7d %-16.2f %-18.2f %-14d %-16d %-13d %-20d\n", current_weeks, porcent_immune,porcent_infected,alive_turtles, alive_turtles-infected_counter-immune_counter,infected_counter,immune_counter );
    //Setup all turtles at the start depending on input values
    setup_turles(turtles);
    //Simulate for x number for weeks
    while(simulation_duration > 0){
        infected_counter = 0;
        immune_counter = 0;
        alive_turtles = 0;
        //Stuff for each thread to manage
        for(int i = 0; i<NUMBER_THREADS-4; i++) {
            create_result = pthread_create(&thread[i], NULL, simulation, (void *) &thread_data_array[i]);
            assert(!create_result);
        }
        //End
        for(int i = 0; i< NUMBER_THREADS-4; i++) {
            join_result = pthread_join(thread[i], NULL); 
            assert(!join_result);
        }
        //3 diferent threads to count each different value needed 
        create_result = pthread_create(&thread[4], NULL, count_alive, NULL);
        assert(!create_result);
        create_result = pthread_create(&thread[5], NULL, count_immune, NULL);
        assert(!create_result);
        create_result = pthread_create(&thread[6], NULL, count_sick, NULL);
        assert(!create_result);
        //Wait for all threads to finish counting before continue
        for (int i = 4; i < 7; i++)
        {   
            join_result = pthread_join(thread[i], NULL);
            assert(!join_result);
        }
        //Calculate results and print out to output files
        simulation_duration--;
        current_weeks++;
        porcent_immune = ((float)immune_counter/(float)alive_turtles)*100;
        porcent_infected = ((float)infected_counter/(float)alive_turtles)*100;
        fprintf(csv_out, " %d,%f,%f,", current_weeks, porcent_immune,porcent_infected );
        fprintf(f_out, "%-7d %-16.2f %-18.2f %-14d %-16d %-13d %-20d\n", current_weeks, porcent_immune,porcent_infected,alive_turtles, alive_turtles-infected_counter-immune_counter,infected_counter,immune_counter );
        fprintf(csv_out, "%d,%d,%d,%d\n", alive_turtles, alive_turtles-infected_counter-immune_counter,infected_counter,immune_counter);

    }
    pthread_mutex_destroy(&lock);
    pthread_exit(NULL);
}