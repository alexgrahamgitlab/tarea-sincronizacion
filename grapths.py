#!/usr/bin/env python3
import matplotlib.pyplot as plt 
import numpy as np 
import pandas as pd 
import os 
import pprint 
import csv 


def main():
    file = pd.read_csv('cvs_out.txt') 
    plt.plot(file["WEEKS"],file["TOTAL PEOPLE"],'b-',label ='TOTAL PEOPLE')
    plt.plot(file["WEEKS"], file["HEALTHY PEOPLE"],'g-',label = 'HEALTHY PEOPLE')
    plt.plot(file["WEEKS"],file["SICK PEOPLE"],'r-',label = 'SICK PEOPLE')
    plt.plot(file["WEEKS"],file["IMMUNE PEOPLE"],'k-', label='IMMUNE PEOPLE')
    plt.ylim(0,350)
    
    plt.legend(loc="upper left")
    plt.xlabel('Weeks')
    plt.ylabel('People')
    plt.show()

if __name__ == '__main__':
    main()