#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <stdbool.h>
#include <time.h>
#define MAX_POP 300
#define LIFESPAN 50*52
#define chance_reproduce 10
#define IMMUNITY_DURATION 52
#define CARRYING_CAPACITY 300
#define DEAD 1
#define IMMUNE 0
#define NOT_IMMUNE 2
/* Global Variables */
double porcent_infected;
double porcent_immune;
int number_people;
int chance_recover;
int duration; 
int infectiousness; 
int immune_counter;
int infected_counter;
int alive_turtles;
int simulation_duration;
/* Entities estructure */
struct turtle {
    bool sick;
    int remaining_immunity;
    int sick_time;
    int age; 
    //Extra
    int x_loc;
    int y_loc;
    bool alive; 
}; 
//turtles can get sick 
void get_sick (struct turtle * turtles) {
    turtles->sick = true; 
    turtles->remaining_immunity = 0;
} 
/* Fuction used to setup a number of turles at the start of the program*/
void setup_turles (struct turtle * turtles) {
    for(int i = 0; i < number_people; i++) {
        turtles[i].x_loc = rand()%100;
        turtles[i].y_loc = rand()%100;
        turtles[i].age = rand()%LIFESPAN;
        turtles[i].sick_time = 0;
        turtles[i].remaining_immunity = 0;
        turtles[i].alive = true; 
        turtles[i].sick = false; 
    }
    for(int i = number_people; i< MAX_POP; i++) {
        turtles[i].alive = false;
        turtles[i].sick = false;
    }
    //Randomly get 10 turtles sick
    for(int i = 0; i < 10; i++) {
        get_sick(&turtles[rand()%number_people]);
        
    }
}
//Turtles get healthy
void get_healthy (struct turtle * turtles) {
    turtles->sick = false; 
    turtles->remaining_immunity = 0;
    turtles->sick_time = 0;
}

//turtles can become immune for a set period of time if they survive
void become_immune (struct turtle * turtles) {
    turtles->sick = false; 
    turtles->sick_time = 0;
    turtles->remaining_immunity = IMMUNITY_DURATION;
}
/* When turtles get older, they can die and update their timers.*/
void get_older (struct turtle * turtles) {
    turtles->age = turtles->age + 1;
    if(turtles->age > LIFESPAN) {
        turtles->alive = false; 
    } else {
        if(turtles->remaining_immunity > 0) {
            turtles->remaining_immunity--;  
        } 
        if(turtles->sick) {
            turtles->sick_time++;
        }
    }
    
}
//Fuction moves a turtle randomly
void move_turtle(struct turtle * turtles) {
    turtles->x_loc = rand()%35;
    turtles->y_loc = rand()%35;
}
int check_immunity(struct turtle turtles) {
    if(turtles.remaining_immunity > 0) {
        return IMMUNE;
    } else return NOT_IMMUNE;
}
//function takes in a sick turtle and infects nearby turtles on the same coordinates by chance
void infect(struct turtle * turtles, struct turtle * sick_turtle) {
    for (int i = 0 ; i < MAX_POP; i++) {
        if (!turtles[i].sick) {     
            if(sick_turtle->x_loc == turtles[i].x_loc && sick_turtle->y_loc == turtles[i].y_loc && check_immunity(turtles[i]) == NOT_IMMUNE) {
                if(rand()%100 < infectiousness) {
                    get_sick(&turtles[i]);
                }
            }
        }
    }
}

//Fuction initializes a new turtle with new parameters
void hatch(struct turtle * turtles) {
    for (int i = 0; i < MAX_POP; i++) {
        if(!turtles[i].alive) {
            //Set up turtle as a new turtle with age 1
            turtles[i].alive = true;
            turtles[i].age = 1;
            //Make new turtle healthy
            get_healthy(&turtles[i]);
            turtles[i].x_loc = rand()%35;
            turtles[i].y_loc = rand()%35;
            //Increment total of alive turtles
            break;
        }
    }
}

//turtles reproduce randomly by chance 
void reproduce(struct turtle * turtles) {
    if (alive_turtles < MAX_POP && ((float)rand()/RAND_MAX)*100 < 1) {
        //hatch(turtles);
        for (int i = 0; i < MAX_POP; i++) {
        if(!turtles[i].alive) {
            //Set up turtle as a new turtle with age 1
            turtles[i].alive = true;
            turtles[i].age = 1;
            //Make new turtle healthy
            get_healthy(&turtles[i]);
            turtles[i].x_loc = rand()%35;
            turtles[i].y_loc = rand()%35;
            //Increment total of alive turtles
            break;
        }
    }
    }
}

int recover_or_die(struct turtle* turtles) {
    if(turtles->sick_time > duration) {
        if(((float)rand()/RAND_MAX)*100 < chance_recover) {
            //if turtle survives virus and recoves he becomes immune
            become_immune(turtles);
            return IMMUNE;
        } else {
            //turtles dies if it doeesnt surive
            turtles->alive = false; 
            return DEAD;
        }

    }
    return 0;
}
int main(int argc, char * argv []) {
    struct turtle turtles[MAX_POP];
    int current_weeks = 0;
    immune_counter = 0;
    infected_counter = 0;
    FILE *csv_out;
    FILE *f_out;
    csv_out = fopen("cvs_out.txt", "w");
    f_out = fopen("sim_out.txt", "w");
    srand(time(NULL));
    //Set up constants
    if (argc > 1) {
        number_people = atoi(argv[2]);
        infectiousness = atoi(argv[4]);
        chance_recover = atoi(argv[6]);
        duration = atoi(argv[8]);
        //Duration of simulation es in weeks
        simulation_duration = atoi(argv[10]); 
    }
    fprintf(f_out, "                                       RESULTADOS SIMULACION                                           \n");
    fprintf(csv_out, "WEEKS,PERCENT IMMUNE,PERCENT INFECTED,TOTAL PEOPLE,HEALTHY PEOPLE,SICK PEOPLE,IMMUNE PEOPLE\n");
    fprintf(f_out, "%-7s %-16s %-18s %-14s %-16s %-13s %-20s\n","WEEKS","PERCENT IMMUNE","PERCENT INFECTED","TOTAL PEOPLE","HEALTHY PEOPLE","SICK PEOPLE","IMMUNE PEOPLE");
    alive_turtles = number_people; 
    setup_turles(turtles);
    porcent_immune = (float)immune_counter/(float)alive_turtles;
    porcent_infected = (float)infected_counter/(float)alive_turtles;
    fprintf(csv_out, "%d,%f,%f,", current_weeks, porcent_immune,porcent_infected );
    fprintf(csv_out, "%d,%d,%d,%d\n", alive_turtles, alive_turtles-infected_counter-immune_counter,infected_counter,immune_counter);
    fprintf(f_out, "%-7d %-16.2f %-18.2f %-14d %-16d %-13d %-20d\n", current_weeks, porcent_immune,porcent_infected,alive_turtles, alive_turtles-infected_counter-immune_counter,infected_counter,immune_counter );
    
    while(simulation_duration > 0) {
        infected_counter = 0;
        immune_counter = 0;
        for(int i = 0; i < MAX_POP; i++) {
            
            if(turtles[i].alive){
                get_older(&turtles[i]);
                move_turtle(&turtles[i]);
                if(turtles[i].sick) {
                    recover_or_die(&turtles[i]);
                    infect(turtles, &turtles[i]);
                }
                else {
                    reproduce(turtles);
                }
            }
        }
        alive_turtles = 0;
        for(int i = 0; i < MAX_POP; i++){
            if(turtles[i].sick && turtles[i].alive) {
                infected_counter++;
                alive_turtles++;
            } else {
                if(turtles[i].remaining_immunity > 0 && turtles[i].alive) {
                    immune_counter++;
                    alive_turtles++;
                } else {
                    if(turtles[i].alive) {
                        alive_turtles++;
                    }
                }
            }
        }


        int actual_alive = 0;
        for(int i = 0; i<MAX_POP; i++) {
            if(turtles[i].alive) {
                actual_alive++;
            }
        }
        simulation_duration--;
        current_weeks++;
        porcent_immune = ((float)immune_counter/(float)alive_turtles)*100;
        porcent_infected = ((float)infected_counter/(float)alive_turtles)*100;
        fprintf(csv_out, " %d,%f,%f,", current_weeks, porcent_immune,porcent_infected );
        fprintf(f_out, "%-7d %-16.2f %-18.2f %-14d %-16d %-13d %-20d\n", current_weeks, porcent_immune,porcent_infected,alive_turtles, alive_turtles-infected_counter-immune_counter,infected_counter,immune_counter );
        fprintf(csv_out, "%d,%d,%d,%d\n", alive_turtles, alive_turtles-infected_counter-immune_counter,infected_counter,immune_counter);
    }

    
}